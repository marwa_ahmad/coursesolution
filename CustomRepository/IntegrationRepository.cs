﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Data.GenericContext;
using Model.Contracts;

namespace CustomRepository
{
    public class IntegrationRepository : IIntegrationRepository
    {
        private readonly IGenericsDbContextRepository<Models.Course> _courseDbContext;
        private readonly IMapper _mapper;


        public IntegrationRepository(IMapper mapper, IGenericsDbContextRepository<Models.Course> courseDbContext)
        {
            _mapper = mapper;
            _courseDbContext = courseDbContext;
        }
        public IEnumerable<GetCourseStudentTeacherModel> GetDetails()
        {
            var courses = _courseDbContext.GetAll();
            if (courses == null || courses.Any() == false) return null;
            var result = new List<GetCourseStudentTeacherModel>();
            foreach (var c in courses)
            {
                var currentResult = new GetCourseStudentTeacherModel()
                {
                    Course = GetCourses(c),
                    Students = GetStudents(c),
                    Teachers = GetTeachers(c)
                };
                if (currentResult != null)
                {
                    result.Add(currentResult);
                }
            }            
            return result;
        }

        private IEnumerable<GetTeacherModel> GetTeachers(Models.Course c)
        {
            var teachers = _mapper.Map<ICollection<Models.CourseTeacher>, IEnumerable<GetTeacherModel>>(c.CourseTeachers);            
            return teachers;
        }
        private IEnumerable<GetStudentModel> GetStudents(Models.Course c)
        {
            var students = _mapper.Map<ICollection<Models.CourseStudent>, IEnumerable<GetStudentModel>>(c.CourseStudents);
            return students;            
        }
        private GetCourseStudentSummaryModel GetCourses(Models.Course c)
        {
            var courseStudentsExist = c.CourseStudents != null && c.CourseStudents.Any();
            var studentsAges = courseStudentsExist ? c.CourseStudents.Select(s => s.Student.Age) : null;
            return new GetCourseStudentSummaryModel()
            {
                CourseID = c.Id,
                CourseName = c.Name,
                AssignedStudentsCount = courseStudentsExist ? c.CourseStudents.Count() : 0,
                StudentMaxAge = courseStudentsExist ? studentsAges.Max() : 0,
                StudentMinAge = courseStudentsExist ? studentsAges.Min() : 0,
                TotalCapacity = c.Capacity
            };
        }
    }
}
