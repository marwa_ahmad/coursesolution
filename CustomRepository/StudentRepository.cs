﻿using Data.GenericContext;
using Microsoft.Extensions.Logging;

namespace CustomRepository
{
    public class StudentRepository : IStudentRepository
    {
        private readonly ILogger _logger;

        public StudentRepository(ILogger logger            
            , IGenericsDbContextRepository<Models.Student> studentDbContextRepo)
        {
            _logger = logger;
            GenericsDbContext = studentDbContextRepo;
        }
        public IGenericsDbContextRepository<Models.Student> GenericsDbContext { get; }
    }
}
