﻿using Data.GenericContext;

namespace CustomRepository
{
    public interface ITeacherRepository
    {
        IGenericsDbContextRepository<Models.Teacher> GenericsDbContext { get; }
    }
}
