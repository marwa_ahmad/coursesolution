﻿
namespace CustomRepository
{
    public interface ICourseTeacherRepository
    {
        bool Assign(int courseId, int teacherId);
    }
}
