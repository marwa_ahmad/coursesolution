﻿using Model.Contracts;
using System;
using System.Collections.Generic;

namespace CustomRepository
{
    public interface IIntegrationRepository
    {
        IEnumerable<GetCourseStudentTeacherModel> GetDetails();
    }
}
