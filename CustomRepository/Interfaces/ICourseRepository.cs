﻿using Data.GenericContext;
using System.Collections.Generic;

namespace CustomRepository
{
    public interface ICourseRepository
    {
        IGenericsDbContextRepository<Models.Course> GenericsDbContext { get; }
    }
}
