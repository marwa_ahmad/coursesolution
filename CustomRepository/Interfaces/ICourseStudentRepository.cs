﻿using Model.Contracts;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CustomRepository
{
    public interface ICourseStudentRepository
    {
        /// <summary>
        /// Assign the student to the given course
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="studentId"></param>
        /// <returns>true/false and throws CourseCapacityFullException if exceeded course capacity</returns>
        bool Signup(int courseId, int studentId);
        Task SignupPublishAsync(int courseId, int studentId, CancellationToken cancellationToken);
        Task<bool> SignupSubscribeAsync();
        IEnumerable<GetCourseStudentSummaryModel> GetList();
    }
}
