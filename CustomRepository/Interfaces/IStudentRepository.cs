﻿using Data.GenericContext;
using System.Collections.Generic;

namespace CustomRepository
{
    public interface IStudentRepository
    {
        IGenericsDbContextRepository<Models.Student> GenericsDbContext { get; }
    }
}
