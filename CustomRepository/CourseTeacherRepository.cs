﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using Models;
using System.Linq;
using Data.GenericContext;

namespace CustomRepository
{
    public class CourseTeacherRepository : ICourseTeacherRepository
    {
        private const string INVALID_INPUTS = "Invalid inputs";

        private readonly ILogger _logger;
        private readonly IGenericsDbContextRepository<Models.CourseTeacher> _courseTeacherDbContext;

        public CourseTeacherRepository(ILogger logger
            , IGenericsDbContextRepository<Models.CourseTeacher> courseTeacherDbContext)
        {
            _logger = logger;
            _courseTeacherDbContext = courseTeacherDbContext;
        }
        public bool Assign(int courseId, int teacherId)
        {
            return ProcessAssign(new Models.CourseTeacher()
            {
                TeacherId = teacherId,
                CourseId = courseId,
            });
        }
        private bool ProcessAssign(Models.CourseTeacher courseTeacher)
        {
            try
            {
                if (courseTeacher == null || courseTeacher.CourseId == 0 || courseTeacher.TeacherId == 0)
                {
                    _logger.LogInformation(INVALID_INPUTS);
                    return false;
                }                
                courseTeacher.JoinDate = DateTime.UtcNow;
                _courseTeacherDbContext.Add(courseTeacher);
                var records = _courseTeacherDbContext.SaveChanges();

                return records > 0;
            }
            catch (Exception exp)
            {
                _logger.Log(LogLevel.Error, exp.Message, exp.Source);
                throw;
            }
        }
    }
}
