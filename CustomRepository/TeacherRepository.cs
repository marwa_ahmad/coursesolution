﻿using Data.GenericContext;
using Microsoft.Extensions.Logging;

namespace CustomRepository
{
    public class TeacherRepository : ITeacherRepository
    {

        private readonly ILogger _logger;

        public TeacherRepository(ILogger logger
            , IGenericsDbContextRepository<Models.Teacher> teacherDbContextRepo)
        {
            _logger = logger;
            GenericsDbContext = teacherDbContextRepo;
        }
        public IGenericsDbContextRepository<Models.Teacher> GenericsDbContext { get; }
    }
}
