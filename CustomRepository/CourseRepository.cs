﻿using System;
using System.Collections.Generic;
using System.Text;
using CourseLogger;
using Models;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using RabbitMQServiceBus;
using Data.GenericContext;

namespace CustomRepository
{
    public class CourseRepository : ICourseRepository
    {
        //private const string INVALID_INPUTS = "Invalid inputs";
        //private const string CFREATED_SUCCESSFULLY = "Successful";
        //private const string CFREATED_FAILED = "Not successfull";


        private readonly ILogger _logger;
        private readonly IGenericsDbContextRepository<Models.Course> _courseDbContextRepo;
        private readonly RabbitMQBus _messagingServiceBus;

        public CourseRepository(ILogger logger
            , IGenericsDbContextRepository<Models.Course> courseDbContextRepo
            , RabbitMQBus messagingServiceBus)
        {
            _logger = logger;
            _courseDbContextRepo = courseDbContextRepo;
            _messagingServiceBus = messagingServiceBus;
        }
        public IGenericsDbContextRepository<Models.Course> GenericsDbContext => _courseDbContextRepo;                
        
    }
}
