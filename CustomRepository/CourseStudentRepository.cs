﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using Models;
using System.Linq;
using System.Threading.Tasks;
using RabbitMQServiceBus;
using MessagingProvider;
using Model.Contracts;
using Data.GenericContext;
using System.Threading;

namespace CustomRepository
{
    public class CourseStudentRepository : ICourseStudentRepository
    {
        private const string INVALID_INPUTS = "Invalid inputs";
        private const string CFREATED_SUCCESSFULLY = "Successful";
        private const string CFREATED_FAILED = "Not successfull";

        private readonly ILogger _logger;
        private readonly RabbitMQBus _messagingServiceBus;
        private readonly IGenericsDbContextRepository<Models.CourseStudent> _courseStudentDbContext;
        private readonly IGenericsDbContextRepository<Models.Course> _courseDbContext;
        private readonly IMessageSystem _messageSystem;

        public CourseStudentRepository(ILogger logger
            , IGenericsDbContextRepository<Models.CourseStudent> courseStudentDbContext
            , IGenericsDbContextRepository<Models.Course> courseDbContext
            , IMessageSystem messageSystem
            , RabbitMQBus messagingServiceBus)
        {
            _logger = logger;
            _messagingServiceBus = messagingServiceBus;
            _courseStudentDbContext = courseStudentDbContext;
            _courseDbContext = courseDbContext;
            _messageSystem = messageSystem;
        }

        public IEnumerable<GetCourseStudentSummaryModel> GetList()
        {
            var courses = _courseDbContext.GetAll();
            if (courses == null || courses.Any() == false) return null;
            var result = (from c in courses
                          select new GetCourseStudentSummaryModel()
                          {
                              CourseID = c.Id,
                              CourseName = c.Name,
                              AssignedStudentsCount = c.CourseStudents != null ? c.CourseStudents.Count() : 0,
                              StudentMaxAge = c.CourseStudents != null && c.CourseStudents.Any() ? c.CourseStudents.Select(s => s.Student.Age).Max() : 0,
                              StudentMinAge = c.CourseStudents != null && c.CourseStudents.Any() ? c.CourseStudents.Select(s => s.Student.Age).Min() : 0,
                              TotalCapacity = c.Capacity
                          }).ToList();
            return result;
        }

        public bool Signup(int courseId, int studentId)
        {
            var task = ProcessSignupAsync(new CourseStudent()
            {
                StudentId = studentId,
                CourseId = courseId,
            });
            var result = task.Result; //intensionlly done to block the caller waiting for the async method to finish
            return result;
        }
        public Task SignupPublishAsync(int courseId, int studentId, CancellationToken cancellationToken)
        {
            // Publish creation of course student to messaging service bus queue.
            var courseStudent = new Models.CourseStudent()
            {
                CourseId = courseId,
                StudentId = studentId,
            };            
            var publishTask = Task.Factory.StartNew(
                action: () => _messagingServiceBus.Publish("Course_Student_Create", courseStudent),
                cancellationToken: cancellationToken,
                creationOptions: TaskCreationOptions.PreferFairness,
                scheduler: null               
                );
            return publishTask;
        }

        public async Task<bool> SignupSubscribeAsync()
        {
            var courseStudent = _messagingServiceBus.Subscribe<Models.CourseStudent>("Course_Student_Create");
            var isSignedUp = await ProcessSignupAsync(courseStudent);
            if (isSignedUp)
            {
                _messageSystem.Body = CFREATED_SUCCESSFULLY;
                await _messageSystem.SendAsync();// send it to a queue instead of awaiting it which will handle it
            }
            return isSignedUp;
        }
        
        private async Task<bool> ProcessSignupAsync(Models.CourseStudent courseStudent)
        {
            try
            {
                if (courseStudent == null || courseStudent.CourseId == 0 || courseStudent.StudentId == 0)
                {
                    _logger.LogInformation(INVALID_INPUTS);
                    return false;
                }
                var course = await _courseDbContext.FindAsync(courseStudent.CourseId);

                if (course == null) throw new KeyNotFoundException();
                if (course.CourseStudents.Count() >= course.Capacity) throw new CourseCapacityFullException();

                courseStudent.JoinDate = DateTime.UtcNow;

                _courseStudentDbContext.Add(courseStudent);

                var records = await _courseStudentDbContext.SaveChangesAsync();

                return records > 0;
            }
            catch (Exception exp)
            {
                _logger.Log(LogLevel.Error, exp.Message, exp.Source);
                throw;
            }
        }
    }
}
