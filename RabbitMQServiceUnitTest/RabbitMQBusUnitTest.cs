using RabbitMQServiceBus;
using Xunit;

namespace MessagingBusUnitTest
{
    public class RabbitMQBusUnitTest
    {
        private readonly RabbitMQBus _rabbitMQBus;

        public RabbitMQBusUnitTest()
        {                        
            _rabbitMQBus = new RabbitMQBus();
        }
        [Fact]
        public void Publish_Subscribe_To_RabbitMQ_NotNull()
        {
            var message = "this is from publisher";     
            _rabbitMQBus.Publish("CREATE_MSG", message);
            var publishedMsg = _rabbitMQBus.Subscribe<string>("CREATE_MSG");

            Assert.NotNull(publishedMsg);
        }
    }
}
