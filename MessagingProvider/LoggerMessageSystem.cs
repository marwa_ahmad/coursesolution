﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProvider
{
    public class LoggerMessageSystem : IMessageSystem
    {
        private ILogger _logger;

        public LoggerMessageSystem(ILogger logger)
        {
            _logger = logger;
        }
        public string Email { get; set; }
        public string Body { get; set; }
        public Task SendAsync()
        {
            return Task.Run(()=> {
                _logger.LogInformation(Body);
            });
        }
    }
}
