﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProvider
{
    public class ConsoleMessageSystem : IMessageSystem
    {
        public string Email { get; set; }
        public string Body { get; set; }
        public Task SendAsync()
        {
            Console.WriteLine($"{Email} {Body}");
            return Task.CompletedTask;
        }
    }
}
