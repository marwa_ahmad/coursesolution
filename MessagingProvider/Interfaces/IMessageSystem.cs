﻿using System;
using System.Threading.Tasks;

namespace MessagingProvider
{
    public interface IMessageSystem
    {
        string Email { get; set; }
        string Body { get; set; }
        Task SendAsync();
    }
}
