﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Net;

namespace CourseAPI.GlobalErrorHandling.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        private const string INTERNAL_SERVER_ERROR = "Internal Server Error";
        private const string SOMETHING_WRONG = "Something went wrong: ";

        public static void ConfigureExceptionHandler(this IApplicationBuilder app, ILogger logger)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";

                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        logger.LogError($"{SOMETHING_WRONG}{contextFeature.Error}");

                        await context.Response.WriteAsync(new ErrorDetails()
                        {
                            StatusCode = context.Response.StatusCode,
                            Message = INTERNAL_SERVER_ERROR
                        }.ToString());
                    }
                });
            });
        }
    }
}
