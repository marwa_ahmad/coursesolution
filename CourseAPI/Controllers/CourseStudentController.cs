﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CustomRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model.Contracts;
using RabbitMQServiceBus;
using System.Threading;

namespace CourseAPI.Controllers
{
    //ref: https://docs.microsoft.com/en-us/aspnet/core/performance/caching/response?view=aspnetcore-3.0
    [Route("api/[controller]")]
    [ApiController]
    public class CourseStudentController : ControllerBase
    {
        private const string Internal_Server_Error = "InternalServerError";
        private readonly ICourseStudentRepository _courseStudentRepo;
        private readonly RabbitMQBus _mqServiceBus;
        private readonly ILogger _logger;

        public CourseStudentController(
            ICourseStudentRepository courseStudentRepository, 
            ILogger logger,
            RabbitMQBus mqServiceBus)
        {
            _courseStudentRepo = courseStudentRepository;
            _logger = logger;
            _mqServiceBus = mqServiceBus;
        }


        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<GetCourseStudentSummaryModel>> Get()
        {
            var courseStudents = _courseStudentRepo.GetList();
            return Ok(courseStudents);
        }

        // POST api/CourseStudent
        //If NoStore is false and Location is None, Cache-Control, and Pragma are set to no-cache.
        //NoStore is typically set to true for error pages.        
        [HttpPost]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ActionResult Sigup(int courseId, int studentId)
        {
            try
            {
                var isSignedUp = _courseStudentRepo.Signup(courseId: courseId, studentId: studentId);
                if (isSignedUp)
                {
                    return Ok();
                }
                return BadRequest(Internal_Server_Error);
            }
            catch (CourseCapacityFullException e)
            {
                return BadRequest(HandleCourseCapacityFullException(courseId, studentId));
            }
        }

        [HttpPost]
        [ResponseCache(CacheProfileName = StringMessages.DefaultCacheProfile)] //VaryByHeader = "User-Agent", Duration = 30)]
        public async Task<ActionResult> SigupAsync(int courseId, int studentId, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {                
                await _courseStudentRepo.SignupPublishAsync(courseId: courseId, studentId: studentId, cancellationToken: cancellationToken);
                var isSignedUp = await _courseStudentRepo.SignupSubscribeAsync();
                if (isSignedUp) return Ok();
                return BadRequest(Internal_Server_Error);
            }
            catch (CourseCapacityFullException e)
            {
                return BadRequest(HandleCourseCapacityFullException(courseId, studentId));
            }
        }
        private string HandleCourseCapacityFullException(int courseId, int studentId)
        {
            var msg = $"Signup is full: CourseId {courseId} StudentId {studentId}";
            _logger.LogWarning(msg);
            return msg;
        }

    }
}
