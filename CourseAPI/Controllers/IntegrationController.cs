﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model.Contracts;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CourseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IntegrationController : ControllerBase
    {
        private readonly IIntegrationRepository _courseStudentTeacherRepo;
        
        public IntegrationController(
            IIntegrationRepository courseStudentTeacherRepo)
        {
            _courseStudentTeacherRepo = courseStudentTeacherRepo;
        }

        [HttpGet]
        public ActionResult<IEnumerable<GetCourseStudentTeacherModel>> Get()
        {
            var courseStudentsTeachers = _courseStudentTeacherRepo.GetDetails();
            return Ok(courseStudentsTeachers);
        }
    }
}
