using Xunit;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using CustomRepository;
using Data.GenericContext;

namespace CustomRepository.Test
{
    public class StudentRepositoryUnitTest : IClassFixture<TestSetup_Student>
    {
        private IStudentRepository _studentRepository;
        private ServiceProvider _serviceProvider;


        public StudentRepositoryUnitTest(TestSetup_Student testSetup)
        {
            _serviceProvider = testSetup.ServiceProvider;

            var logger = _serviceProvider.GetService<ILogger>();
            var dbConext = _serviceProvider.GetService<DbContext>();
            var studentDbContext = _serviceProvider.GetService<IGenericsDbContextRepository<Models.Student>>();

            _studentRepository = _serviceProvider.GetService<IStudentRepository>();
        }

        [Fact]
        public void CreateStudent_NotNull()
        {
            var student = new Models.Student()
            {
                Name = "Test",
            };
            var studentAdded = _studentRepository.GenericsDbContext.Add(student);
            _studentRepository.GenericsDbContext.SaveChanges();
            Assert.NotNull(studentAdded);
        }
    }
}
