using Xunit;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQServiceBus;
using Data.GenericContext;

namespace CustomRepository.Test
{
    public class CourseRepositoryUnitTest : IClassFixture<TestSetup_Course>
    {
        //look for startup class
        private ICourseRepository _courseRepository;        
        private ServiceProvider _serviceProvider;

        public CourseRepositoryUnitTest(TestSetup_Course testSetup)
        {
            _serviceProvider = testSetup.ServiceProvider;

            var dbConext = _serviceProvider.GetService<DbContext>();
            var mockedLogger = _serviceProvider.GetService<ILogger>();
            var mockedMessagingBus = _serviceProvider.GetService<RabbitMQBus>();
            var courseDbContext = _serviceProvider.GetService<IGenericsDbContextRepository<Models.Course>>();

            _courseRepository = _serviceProvider.GetService<ICourseRepository>();
        }

        [Fact]
        public void Create_Course_NotNull()
        {
            var course = new Models.Course()
            {
                Capacity = 2,
                Name = "TestC1",                
            };
            var addedCourse = _courseRepository.GenericsDbContext.Add(course);
            _courseRepository.GenericsDbContext.SaveChanges();
            Assert.NotEqual(0, addedCourse.Id);
        }
                
        private Models.Course AddNewCourse()
        {
            var course = new Models.Course()
            {
                Capacity = 2,
                Name = "TestC2",
            };
            var addedCourse = _courseRepository.GenericsDbContext.Add(course);
            _courseRepository.GenericsDbContext.SaveChanges();
            return addedCourse;
        }
        
    }
}
