﻿using AutoMapper;
using Data.CustomDbContext;
using Data.GenericContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Model.Contracts;
using Models;
using Moq;
using RabbitMQServiceBus;

namespace CustomRepository.Test
{
    public class TestSetup_Course
    {
        public TestSetup_Course()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<DbContext, CourseContext>();//if we make it transient it will not fetch the linked objects.
            serviceCollection.AddTransient<IGenericsDbContextRepository<Course>, GenericsDbContextRepository<Course>>();

            var mockedRabbitMQ = new Mock<RabbitMQBus>();
            serviceCollection.AddSingleton<RabbitMQBus>(mockedRabbitMQ.Object);

            var mockedLogger = new Mock<ILogger>();
            serviceCollection.AddSingleton<ILogger>(mockedLogger.Object);

            serviceCollection.AddAutoMapper(typeof(AutoMapperProfile));

            //var mapper = AutoMapperConfiguration.ConfigureAutoMapper();
            //serviceCollection.AddSingleton<IMapper>(mapper);

            serviceCollection.AddTransient<ICourseRepository, CourseRepository>();

            ServiceProvider = serviceCollection.BuildServiceProvider();
        }
        public ServiceProvider ServiceProvider { get; private set; }
    }    
}
