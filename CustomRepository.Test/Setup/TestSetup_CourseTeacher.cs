﻿using AutoMapper;
using CustomRepository;
using Data.CustomDbContext;
using Data.GenericContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Models;
using Moq;
using RabbitMQServiceBus;

namespace CustomRepository.Test
{
    public class TestSetup_CourseTeacher
    {
        public TestSetup_CourseTeacher()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<DbContext, CourseContext>();//if we make it transient it will not fetch the linked objects.
            serviceCollection.AddTransient<IGenericsDbContextRepository<Models.Course>, GenericsDbContextRepository<Models.Course>>();
            serviceCollection.AddTransient<IGenericsDbContextRepository<Models.Teacher>, GenericsDbContextRepository<Models.Teacher>>();
            serviceCollection.AddTransient<IGenericsDbContextRepository<Models.CourseTeacher>, GenericsDbContextRepository<Models.CourseTeacher>>();

            var mockedLogger = new Mock<ILogger>();
            serviceCollection.AddSingleton<ILogger>(mockedLogger.Object);            

            var mockedRabbitMQBus = new Mock<RabbitMQBus>();
            serviceCollection.AddSingleton<RabbitMQBus>(mockedRabbitMQBus.Object);
            

            serviceCollection.AddTransient<ICourseTeacherRepository, CourseTeacherRepository>();
            serviceCollection.AddTransient<ICourseRepository, CourseRepository>();
            serviceCollection.AddTransient<ITeacherRepository, TeacherRepository>();

            ServiceProvider = serviceCollection.BuildServiceProvider();
        }
        public ServiceProvider ServiceProvider { get; private set; }
    }
}
