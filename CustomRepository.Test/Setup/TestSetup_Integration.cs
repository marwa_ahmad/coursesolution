﻿using AutoMapper;
using CustomRepository;
using Data.CustomDbContext;
using Data.GenericContext;
using MessagingProvider;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Model.Contracts;
using Models;
using Moq;
using RabbitMQServiceBus;

namespace CustomRepository.Test
{
    public class TestSetup_Integration
    {
        public TestSetup_Integration()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<DbContext, CourseContext>();//if we make it transient it will not fetch the linked objects.
            serviceCollection.AddTransient<IGenericsDbContextRepository<Models.Course>, GenericsDbContextRepository<Models.Course>>();
            serviceCollection.AddTransient<IGenericsDbContextRepository<Models.Teacher>, GenericsDbContextRepository<Models.Teacher>>();
            serviceCollection.AddTransient<IGenericsDbContextRepository<Models.Student>, GenericsDbContextRepository<Models.Student>>();
            serviceCollection.AddTransient<IGenericsDbContextRepository<Models.CourseTeacher>, GenericsDbContextRepository<Models.CourseTeacher>>();
            serviceCollection.AddTransient<IGenericsDbContextRepository<Models.CourseStudent>, GenericsDbContextRepository<Models.CourseStudent>>();


            var mockedLogger = new Mock<ILogger>();
            serviceCollection.AddSingleton<ILogger>(mockedLogger.Object);

            var messagingSystem = new LoggerMessageSystem(mockedLogger.Object);
            serviceCollection.AddSingleton<IMessageSystem>(messagingSystem);

            serviceCollection.AddAutoMapper(typeof(AutoMapperProfile));

            var mockedRabbitMQBus = new Mock<RabbitMQBus>();
            serviceCollection.AddSingleton<RabbitMQBus>(mockedRabbitMQBus.Object);
            

            serviceCollection.AddTransient<ICourseTeacherRepository, CourseTeacherRepository>();
            serviceCollection.AddTransient<ICourseStudentRepository, CourseStudentRepository>();
            serviceCollection.AddTransient<IIntegrationRepository, IntegrationRepository>();
            serviceCollection.AddTransient<ICourseRepository, CourseRepository>();
            serviceCollection.AddTransient<ITeacherRepository, TeacherRepository>();
            serviceCollection.AddTransient<IStudentRepository, StudentRepository>();


            ServiceProvider = serviceCollection.BuildServiceProvider();
        }
        public ServiceProvider ServiceProvider { get; private set; }
    }
}
