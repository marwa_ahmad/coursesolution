﻿using CustomRepository;
using Data.CustomDbContext;
using Data.GenericContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Models;
using Moq;

namespace CustomRepository.Test
{
    public class TestSetup_Student
    {
        public TestSetup_Student()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<DbContext, CourseContext>();//if we make it transient it will not fetch the linked objects.
            serviceCollection.AddTransient<IGenericsDbContextRepository<Models.Student>, GenericsDbContextRepository<Models.Student>>();

            var mockedLogger = new Mock<ILogger>();
            serviceCollection.AddSingleton<ILogger>(mockedLogger.Object);
            
            serviceCollection.AddTransient<IStudentRepository, StudentRepository>();

            ServiceProvider = serviceCollection.BuildServiceProvider();
        }
        public ServiceProvider ServiceProvider { get; private set; }
    }
}
