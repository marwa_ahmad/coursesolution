﻿using CustomRepository;
using Data.CustomDbContext;
using Data.GenericContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomRepository.Test
{
    public class TestSetup_Teacher
    {
        public TestSetup_Teacher()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<DbContext, CourseContext>();//if we make it transient it will not fetch the linked objects.
            serviceCollection.AddTransient<IGenericsDbContextRepository<Models.Teacher>, GenericsDbContextRepository<Models.Teacher>>();

            var logger = new Mock<ILogger>();
            serviceCollection.AddSingleton<ILogger>(logger.Object);
           
            serviceCollection.AddTransient<ITeacherRepository, TeacherRepository>();

            ServiceProvider = serviceCollection.BuildServiceProvider();
        }
        public ServiceProvider ServiceProvider { get; private set; }
    }
}
