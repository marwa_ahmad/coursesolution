﻿using AutoMapper;
using CourseLogger;
using CustomRepository;
using Data.CustomDbContext;
using Data.GenericContext;
using MessagingProvider;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Models;
using Moq;
using RabbitMQServiceBus;

namespace CustomRepository.Test
{
    public class TestSetup_CourseStudent
    {
        public TestSetup_CourseStudent()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddTransient<RabbitMQBus, RabbitMQBus>();
            serviceCollection.AddSingleton<DbContext, CourseContext>();//if we make it transient it will not fetch the linked objects.
            serviceCollection.AddTransient<IGenericsDbContextRepository<Models.Course>, GenericsDbContextRepository<Models.Course>>();
            serviceCollection.AddTransient<IGenericsDbContextRepository<Models.Student>, GenericsDbContextRepository<Models.Student>>();
            serviceCollection.AddTransient<IGenericsDbContextRepository<Models.CourseStudent>, GenericsDbContextRepository<Models.CourseStudent>>();

            var logger = FileLogging.Factory.CreateLogger("test");
            serviceCollection.AddSingleton<ILogger>(logger);

            var messagingSystem = new LoggerMessageSystem(logger);
            serviceCollection.AddSingleton<IMessageSystem>(messagingSystem);            

            serviceCollection.AddTransient<ICourseStudentRepository, CourseStudentRepository>();
            serviceCollection.AddTransient<ICourseRepository, CourseRepository>();
            serviceCollection.AddTransient<IStudentRepository, StudentRepository>();            

            ServiceProvider = serviceCollection.BuildServiceProvider();
        }
        public ServiceProvider ServiceProvider { get; private set; }
    }
}
