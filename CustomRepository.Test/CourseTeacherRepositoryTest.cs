using AutoMapper;
using Microsoft.Extensions.Logging;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using RabbitMQServiceBus;
using CustomRepository;
using Data.GenericContext;

namespace CustomRepository.Test
{
    public class CourseTeacherRepositoryTest : IClassFixture<TestSetup_CourseTeacher>
    {
        private ICourseRepository _courseRepository;
        private ITeacherRepository _teacherRepository;
        private ICourseTeacherRepository _courseTeacherRepository;
        private ServiceProvider _serviceProvider;

        public CourseTeacherRepositoryTest(TestSetup_CourseTeacher testSetup)
        {
            _serviceProvider = testSetup.ServiceProvider;
            
            _courseTeacherRepository = _serviceProvider.GetService<ICourseTeacherRepository>();
            _courseRepository = _serviceProvider.GetService<ICourseRepository>();
            _teacherRepository = _serviceProvider.GetService<ITeacherRepository>();
        }

        [Fact]
        public void Assign_Teacher_Course_NotNull()
        {
            var teacher = AddNewTeacher();
            var course = AddNewCourse();
            Assert.NotEqual(0, teacher.Id);
            Assert.NotEqual(0, course.Id);
            var isSignedUp = _courseTeacherRepository.Assign(courseId: course.Id, teacherId: teacher.Id);
            Assert.True(isSignedUp);
        }

        private Models.Course AddNewCourse()
        {
            var course = new Models.Course()
            {
                Capacity = 1,
                Name = "TestCourse",
            };
            var addedCourse = _courseRepository.GenericsDbContext.Add(course);
            _courseRepository.GenericsDbContext.SaveChanges();
            return addedCourse;
        }

        private Models.Teacher AddNewTeacher()
        {
            var teacher = new Models.Teacher()
            {
                Name = "TeacherTest",
            };
            var addedTeacher = _teacherRepository.GenericsDbContext.Add(teacher);
            _teacherRepository.GenericsDbContext.SaveChanges();
            return teacher;
        }

    }
}
