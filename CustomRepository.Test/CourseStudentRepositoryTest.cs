using Microsoft.Extensions.Logging;
using System;
using Xunit;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using RabbitMQServiceBus;
using MessagingProvider;
using CustomRepository;
using Data.GenericContext;
using System.Threading.Tasks;
using System.Threading;

namespace CustomRepository.Test
{
    public class CourseStudentRepositoryTest : IClassFixture<TestSetup_CourseStudent>
    {
        private readonly ICourseStudentRepository _courseStudentRepo;
        private ICourseRepository _courseRepository;
        private IStudentRepository _studentRepository;
        private ServiceProvider _serviceProvider;

        public CourseStudentRepositoryTest(TestSetup_CourseStudent testSetup)
        {
            _serviceProvider = testSetup.ServiceProvider;

            var messagingBus = _serviceProvider.GetService<RabbitMQBus>();
            var dbConext = _serviceProvider.GetService<DbContext>(); 
            var courseDbContext = _serviceProvider.GetService<IGenericsDbContextRepository<Models.Course>>();
            var courseStudentDbContext = _serviceProvider.GetService<IGenericsDbContextRepository<Models.CourseStudent>>(); 
            var studentDbContext = _serviceProvider.GetService<IGenericsDbContextRepository<Models.Student>>();
            var logger = _serviceProvider.GetService<ILogger>();
            var messagingSystem = _serviceProvider.GetRequiredService<IMessageSystem>();
            var mockedMapper = _serviceProvider.GetService<IMapper>();

            _courseStudentRepo = _serviceProvider.GetService<ICourseStudentRepository>();
            _courseRepository = _serviceProvider.GetService<ICourseRepository>();
            _studentRepository = _serviceProvider.GetService<IStudentRepository>();
        }

        [Fact]
        public void SignUp_WithinLimit_True()
        {
            var student = AddNewStudent();
            var course = AddNewCourse(2);
            Assert.NotEqual(0, student.Id);
            Assert.NotEqual(0, course.Id);
            var isSignedUp = _courseStudentRepo.Signup(courseId: course.Id, studentId: student.Id);
            Assert.True(isSignedUp);
        }

        [Fact]
        public void SignUp_ExceedLimit_CapacityFullException()
        {
            var student = AddNewStudent();
            var course = AddNewCourse(1);
            Assert.NotEqual(0, student.Id);
            Assert.NotEqual(0, course.Id);
            var isSignedUp = _courseStudentRepo.Signup(courseId: course.Id, studentId: student.Id);
            Assert.True(isSignedUp);

            var student2 = AddNewStudent();
            Action actionSignUp = () => _courseStudentRepo.Signup(courseId: course.Id, studentId: student2.Id);
            Assert.Throws<CourseCapacityFullException>(actionSignUp);
        }

        [Fact]
        public async Task SignUp_Publish_Subscribe_WithinLimit_True()
        {
            var student = AddNewStudent();
            var course = AddNewCourse(2);
            Assert.NotEqual(0, student.Id);
            Assert.NotEqual(0, course.Id);
            var cancelationSource = new CancellationTokenSource();
            await _courseStudentRepo.SignupPublishAsync(courseId: course.Id, studentId: student.Id, cancellationToken: cancelationSource.Token);
            var isSignedUpSubscribed = await _courseStudentRepo.SignupSubscribeAsync();
            Assert.True(isSignedUpSubscribed);
        }


        [Fact]
        public void GetList_NotEmpty()
        {
            var student = AddNewStudent();
            var course = AddNewCourse(2);
            Assert.NotEqual(0, student.Id);
            Assert.NotEqual(0, course.Id);
            var courseStudentsSummary = _courseStudentRepo.GetList();
            Assert.NotEmpty(courseStudentsSummary);
        }
        private Models.Course AddNewCourse(int capacity)
        {
            var course = new Models.Course()
            {
                Capacity = capacity,
                Name = "TestC2",
            };
            var addedCourse = _courseRepository.GenericsDbContext.Add(course);
            _courseRepository.GenericsDbContext.SaveChanges();
            return addedCourse;
        }
        private Models.Student AddNewStudent()
        {
            var student = new Models.Student()
            {
                Name = "Test",
            };
            var studentAdded = _studentRepository.GenericsDbContext.Add(student);
            _studentRepository.GenericsDbContext.SaveChanges();
            return studentAdded;
        }
    }
}
