﻿using Microsoft.Extensions.Logging;
using System;
using Xunit;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using RabbitMQServiceBus;
using MessagingProvider;
using CustomRepository;
using Data.GenericContext;

namespace CustomRepository.Test
{
    public class IntegrationRepositoryTest : IClassFixture<TestSetup_Integration>
    {
        private readonly IIntegrationRepository _integrationRepo;
        private readonly ICourseStudentRepository _courseStudentRepo;
        private readonly ICourseRepository _courseRepository;
        private readonly ITeacherRepository _teacherRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly ICourseTeacherRepository _courseTeacherRepository;
        private readonly ServiceProvider _serviceProvider;


        public IntegrationRepositoryTest(TestSetup_Integration testSetup)
        {
            _serviceProvider = testSetup.ServiceProvider;            

            _integrationRepo = _serviceProvider.GetService<IIntegrationRepository>();
            _courseRepository = _serviceProvider.GetService<ICourseRepository>();
            _studentRepository = _serviceProvider.GetService<IStudentRepository>();
            _courseTeacherRepository = _serviceProvider.GetService<ICourseTeacherRepository>();
            _courseStudentRepo = _serviceProvider.GetService<ICourseStudentRepository>();
            _teacherRepository = _serviceProvider.GetService<ITeacherRepository>();
        }

        [Fact]
        public void GetList_NotEmpty()
        {
            var student = AddNewStudent();
            var course = AddNewCourse(2);
            var teacher = AddNewTeacher();

            Assert.NotEqual(0, student.Id);
            Assert.NotEqual(0, course.Id);
            Assert.NotEqual(0, teacher.Id);

            var isAssigned = _courseTeacherRepository.Assign(courseId: course.Id, teacherId: teacher.Id);
            Assert.True(isAssigned);

            var isSignedUp = _courseStudentRepo.Signup(courseId: course.Id, studentId: student.Id);
            Assert.True(isSignedUp);

            var coursesStudentsTeachersModel = _integrationRepo.GetDetails();
            Assert.NotEmpty(coursesStudentsTeachersModel);
        }
        private Models.Teacher AddNewTeacher()
        {
            var teacher = new Models.Teacher()
            {
                Name = "TeacherTest",
            };
            var addedTeacher = _teacherRepository.GenericsDbContext.Add(teacher);
            _teacherRepository.GenericsDbContext.SaveChanges();
            return teacher;
        }
        private Models.Course AddNewCourse(int capacity)
        {
            var course = new Models.Course()
            {
                Capacity = capacity,
                Name = "TestC2",
            };
            var addedCourse = _courseRepository.GenericsDbContext.Add(course);
            _courseRepository.GenericsDbContext.SaveChanges();
            return addedCourse;
        }
        private Models.Student AddNewStudent()
        {
            var student = new Models.Student()
            {
                Name = "Test",
            };
            var studentAdded = _studentRepository.GenericsDbContext.Add(student);
            _studentRepository.GenericsDbContext.SaveChanges();
            return studentAdded;
        }
    }
}
