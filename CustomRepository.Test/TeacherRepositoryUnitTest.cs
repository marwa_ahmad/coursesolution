using System;
using Xunit;
using Moq;
using Microsoft.Extensions.Logging;
using Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using CustomRepository;
using Data.GenericContext;

namespace CustomRepository.Test
{
    public class TeacherRepositoryUnitTest : IClassFixture<TestSetup_Teacher>
    {
        private ITeacherRepository _teacherRepository;
        private ServiceProvider _serviceProvider;

        public TeacherRepositoryUnitTest(TestSetup_Teacher testSetup)
        {
            _serviceProvider = testSetup.ServiceProvider;

            var logger = _serviceProvider.GetService<ILogger>();
            var dbConext = _serviceProvider.GetService<DbContext>();
            var teacherDbContext = _serviceProvider.GetService<IGenericsDbContextRepository<Models.Teacher>>();

            _teacherRepository = _serviceProvider.GetService<ITeacherRepository>();
        }

        [Fact]
        public void CreateTeacher_NotNull()
        {
            var teacher = new Models.Teacher()
            {
                Name = "TeacherTest",
            };
            var addedTeacher = _teacherRepository.GenericsDbContext.Add(teacher);
            _teacherRepository.GenericsDbContext.SaveChanges();
            Assert.NotNull(addedTeacher);
        }
    }
}
