﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.IO;
using System.Text;

namespace RabbitMQServiceBus
{
    public class RabbitMQBus
    {
        private string _body;
        private object _jsonDeser;
        private readonly ILogger _logger;
        private readonly IConfigurationRoot _config;
        public RabbitMQBus()
        {
            try
            {
                _logger = new LoggerFactory().CreateLogger("RabbitMQBus");
                _config = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json").Build();
            }
            catch (IOException ioExp)
            {
                _logger.Log(LogLevel.Error, ioExp.Message);
            }
            catch (Exception exp)
            {
                _logger.Log(LogLevel.Error, exp.Message);
            }
        }

        /// <summary>
        /// Publishes object to queue.
        /// </summary>
        /// <param name="qName">Queue name.</param>
        /// <param name="data">Data object to be placed into the queue.</param>
        public void Publish(string qName, object data)
        {
            var connectionFactory = new ConnectionFactory();
            _config.GetSection("RabbitMqConnection").Bind(connectionFactory);

            _logger.Log(LogLevel.Information, "Before connection");
            using (var connection = connectionFactory.CreateConnection())
            {
                _logger.LogInformation("In connection");
                using (var model = connection.CreateModel())
                {
                    _logger.LogInformation("In model");

                    model.QueueDeclare(queue: qName,
                                       durable: true,
                                       exclusive: false,
                                       autoDelete: false,
                                       arguments: null);
                    var basicProperties = model.CreateBasicProperties();
                    basicProperties.Persistent = true;

                    //_logger.LogInformation("Data before json", data);

                    var jsonObj = JsonConvert.SerializeObject(data);
                    var dataBuffer = Encoding.UTF8.GetBytes(jsonObj);
                    model.BasicPublish(exchange: string.Empty,
                                       routingKey: qName,
                                       basicProperties: basicProperties,
                                       body: dataBuffer);
                }
            }
        }

        /// <summary>
        /// Consumes/Subscribes queue and get published data.
        /// </summary>
        /// <typeparam name="T">Type of consumed object.</typeparam>
        /// <param name="qName">Queue name.</param>
        /// <returns>Returns consumed object result.</returns>
        public T Subscribe<T>(string qName)
        {
            var connectionFactory = new ConnectionFactory();
            _config.GetSection("RabbitMqConnection").Bind(connectionFactory);

            using (var connection = connectionFactory.CreateConnection())
            {
                using (var model = connection.CreateModel())
                {
                    model.QueueDeclare(queue: qName,
                                       durable: true,
                                       exclusive: false,
                                       autoDelete: false,
                                       arguments: null);
                    BasicGetResult consumer = model.BasicGet(qName, true);
                    if (consumer != null)
                    {
                        _body = Encoding.UTF8.GetString(consumer.Body);
                        _jsonDeser = JsonConvert.DeserializeObject<T>(_body);
                    }
                }
            }

            return (T)_jsonDeser;
        }
    }
}
