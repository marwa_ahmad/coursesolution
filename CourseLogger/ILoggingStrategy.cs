﻿
namespace CourseLogger
{
    public interface ILoggingStrategy
    {
        void Log(string message);
    }
}
