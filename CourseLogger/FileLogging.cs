﻿using Microsoft.Extensions.Logging;
using System.IO;
using System.Reflection;
using Serilog;

namespace CourseLogger
{
    public static class FileLogging
    {
        public static readonly string PhysicalPath;
        public static LoggerFactory Factory = new LoggerFactory();
        private static readonly string _fileName;
        static FileLogging()
        {
            var date = System.DateTime.Today;
            _fileName = Assembly.GetExecutingAssembly().GetName().Name;
            PhysicalPath = Path.Combine($"logs", _fileName);
            Factory.AddSerilog();
            Factory.AddFile(PhysicalPath);
        }
    }
}
