﻿using System;
using Microsoft.Extensions.Logging;

namespace CourseLogger
{
    public class GenericLogger : ILogger
    {
        
        public GenericLogger(string fileName = @"CourseSolution-{Date}.txt")
        {
        }
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            var message = $"{logLevel.ToString()}: {eventId.Id} - {formatter(state, exception)}";
        }
    }
}
