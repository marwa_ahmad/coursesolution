﻿using System.Collections.Generic;

namespace Models
{
    public class Teacher
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<CourseTeacher> CourseTeachers { get; set; }

    }
}
