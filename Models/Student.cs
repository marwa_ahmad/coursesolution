﻿using System.Collections.Generic;

namespace Models
{
    public class Student
    {
        public Student()
        {
            CourseStudents = new HashSet<CourseStudent>();
        }
        public int Id { get; set; }
        public int Age { get; set; }
        public string Name { get; set; }
        public ICollection<CourseStudent> CourseStudents { get; set; }
    }
}
