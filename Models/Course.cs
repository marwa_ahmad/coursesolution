﻿using System.Collections.Generic;

namespace Models
{
    public class Course
    {
        public Course()
        {
            CourseStudents = new HashSet<CourseStudent>();
            CourseTeachers = new HashSet<CourseTeacher>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Capacity { get; set; }
        public ICollection<CourseStudent> CourseStudents { get; set; }
        public ICollection<CourseTeacher> CourseTeachers { get; set; }
    }
}
