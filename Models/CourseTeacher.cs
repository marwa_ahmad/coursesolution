﻿using System;

namespace Models
{
    public class CourseTeacher
    {
        public int CourseId { get; set; }
        public int TeacherId { get; set; }
        public DateTime JoinDate { get; set; }
        public Teacher Teacher { get; set; }
        public Course Course { get; set; }
    }
}
