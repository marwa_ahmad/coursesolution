﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Models;
using System.IO;

namespace Data.CustomDbContext
{
    public partial class CourseContext : DbContext
    {
        public CourseContext()
        {
        }

        public CourseContext(DbContextOptions<CourseContext> options)
            : base(options)
        {
        }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }

        public DbSet<CourseTeacher> CourseTeacher { get; set; }
        public DbSet<CourseStudent> CourseStudent { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured) return;

            //TODO: remove the forward slash
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
            //.SetBasePath(Directory.GetParent(@"./").FullName)

            optionsBuilder.UseSqlServer(configuration.GetConnectionString("CourseDatabase"));            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
                entity.Property(e => e.Name).HasMaxLength(250).IsRequired(true);
                entity.Property(e => e.Capacity).IsRequired(true);
            });
            modelBuilder.Entity<Teacher>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
                entity.Property(e => e.Name).HasMaxLength(250).IsRequired(true);                
            });
            modelBuilder.Entity<Student>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
                entity.Property(e => e.Name).HasMaxLength(250).IsRequired(true);
            });

            modelBuilder.Entity<CourseStudent>().HasKey(sc => new { sc.StudentId, sc.CourseId });

            modelBuilder.Entity<CourseStudent>()
            .HasOne<Student>(sc => sc.Student)
            .WithMany(s => s.CourseStudents)
            .HasForeignKey(sc => sc.StudentId)
            .HasConstraintName("FK_CourseStudent_Student");

            modelBuilder.Entity<CourseStudent>()
                .HasOne<Course>(sc => sc.Course)
                .WithMany(s => s.CourseStudents)
                .HasForeignKey(sc => sc.CourseId)
                .HasConstraintName("FK_CourseStudent_Course");

            modelBuilder.Entity<CourseTeacher>().HasKey(sc => new { sc.TeacherId, sc.CourseId });

            modelBuilder.Entity<CourseTeacher>()
              .HasOne<Course>(sc => sc.Course)
              .WithMany(s => s.CourseTeachers)
              .HasForeignKey(sc => sc.CourseId)
              .HasConstraintName("FK_CourseTeacher_Course");

            modelBuilder.Entity<CourseTeacher>()
                .HasOne<Teacher>(sc => sc.Teacher)
                .WithMany(s => s.CourseTeachers)
                .HasForeignKey(sc => sc.TeacherId)
                .HasConstraintName("FK_CourseTeacher_Teacher");
        }
    }
}
