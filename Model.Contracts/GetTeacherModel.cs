﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Contracts
{
    public class GetTeacherModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
