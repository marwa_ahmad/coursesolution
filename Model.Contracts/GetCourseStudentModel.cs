﻿
namespace Model.Contracts
{
    public class GetCourseStudentSummaryModel
    {
        public int CourseID { get; set; }
        public string CourseName { get; set; }
        public int TotalCapacity { get; set; }
        public int AssignedStudentsCount { get; set; }
        public int StudentMaxAge { get; set; }
        public int StudentMinAge { get; set; }
        public int StudentAvgAge
        {
            get
            {
                if (StudentMinAge != 0 && StudentMaxAge != 0)
                {
                    return (StudentMaxAge + StudentMinAge) / 2;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
