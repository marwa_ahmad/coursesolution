﻿using AutoMapper;
using Models;
namespace Model.Contracts
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {            
            CreateMap<CourseTeacher, GetTeacherModel>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.TeacherId))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Teacher != null ? s.Teacher.Name : null));

            CreateMap<CourseStudent, GetStudentModel>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.StudentId))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Student != null ? s.Student.Name : null))
                .ForMember(d => d.Age, opt => opt.MapFrom(s => s.Student != null ? s.Student.Age : 0));
        }
    }
}
