﻿namespace Model.Contracts
{
    public class GetStudentModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
