﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Contracts
{
    public class GetCourseStudentTeacherModel
    {
        public GetCourseStudentSummaryModel Course { get; set; }
        public IEnumerable<GetStudentModel> Students { get; set; }
        public IEnumerable<GetTeacherModel> Teachers { get; set; }

    }
}
