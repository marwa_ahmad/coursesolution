﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Data.GenericContext
{
    /// <summary>
    /// CRUD operations repository on a Generic type.
    /// </summary>
    /// <typeparam name="TEntity">Entity type.</typeparam>
    public class GenericsDbContextRepository<TEntity> : IGenericsDbContextRepository<TEntity> where TEntity: class
    {
        private DbContext _dbContext;
        public GenericsDbContextRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IDbContextTransaction DbContextTransaction => _dbContext.Database.BeginTransaction();

        public TEntity Add(TEntity entity)
        {
            return _dbContext.Set<TEntity>().Add(entity).Entity;
        }

        public Task AddAsync(TEntity entity)
        {
            return _dbContext.Set<TEntity>().AddAsync(entity);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public TEntity Find(int id)
        {
            return _dbContext.Set<TEntity>().Find(id);
        }
        public Task<TEntity> FindAsync(int id)
        {
            return _dbContext.Set<TEntity>().FindAsync(id);
        }
        public TEntity Find(params object[] ids)
        {
            return _dbContext.Set<TEntity>().Find(ids);
        }
        public Task<TEntity> FindAsync(params object[] ids)
        {
            return _dbContext.Set<TEntity>().FindAsync(ids);
        }
        public TEntity Find(Func<TEntity, bool> predicate)
        {
            return _dbContext.Set<TEntity>().Find(predicate);
        }
        public Task<TEntity> FindAsync(Func<TEntity, bool> predicate)
        {
            return _dbContext.Set<TEntity>().FindAsync(predicate);
        }
        public ICollection<TEntity> GetAll()
        {
            return _dbContext.Set<TEntity>().ToList();
        }

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _dbContext.SaveChangesAsync();
        }
    }
}
