﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.GenericContext
{
    /// <summary>
    /// CRUD operations interface on a Generic type.
    /// </summary>
    /// <typeparam name="TEntity">Target generic type</typeparam>
    public interface IGenericsDbContextRepository<TEntity> : IUnitOfWork, IDisposable where TEntity : class
    {
        TEntity Add(TEntity entity);
        
        Task AddAsync(TEntity entity);
        
        TEntity Find(int id);

        /// <summary>
        /// finds by id asynchronously
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<TEntity> FindAsync(int id);

        /// <summary>
        /// Finds specific entity.
        /// </summary>
        /// <param name="ids">Array of Ids.</param>
        /// <returns>Returns the target entity or null.</returns>
        TEntity Find(params object[] ids);

        Task<TEntity> FindAsync(params object[] ids);


        /// <summary>
        /// Finds specific object using Func delegate.
        /// </summary>
        /// <param name="predicate">Entity predicate.</param>
        /// <returns>Returns entity.</returns>
        TEntity Find(Func<TEntity, bool> predicate);

        Task<TEntity> FindAsync(Func<TEntity, bool> predicate);

        ICollection<TEntity> GetAll();
    }
}
