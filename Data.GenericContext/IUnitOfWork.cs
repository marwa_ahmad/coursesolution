﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace Data.GenericContext
{
    public interface IUnitOfWork
    {
        int SaveChanges();
        
        Task<int> SaveChangesAsync();

        IDbContextTransaction DbContextTransaction { get; }
    }
}
