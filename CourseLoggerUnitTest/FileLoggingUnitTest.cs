using CourseLogger;
using Xunit;
using Microsoft.Extensions.Logging;

namespace CourseLoggerUnitTest
{
    public class FileLoggingUnitTest
    {
        private ILogger _iLogger;
        public FileLoggingUnitTest()
        {
            _iLogger = FileLogging.Factory.CreateLogger("test");
        }
        [Fact]
        public void Log_Information_NotNull()
        {
            _iLogger.LogInformation("Information...");
            Assert.NotNull(_iLogger);
        }

        [Fact]
        public void Log_Exception_NotNull()
        {            
            _iLogger.LogError("Error...");
            Assert.NotNull(_iLogger);
        }

        [Fact]
        public void Log_Debug_NotNull()
        {
            _iLogger.LogDebug("Debug...");
            Assert.NotNull(_iLogger);
        }

        [Fact]
        public void Log_Warning_NotNull()
        {
            _iLogger.LogWarning("Warning...");
            Assert.NotNull(_iLogger);
        }

        [Fact]
        public void Log_Critical_NotNull()
        {
            _iLogger.LogCritical("Critical...");
            Assert.NotNull(_iLogger);
        }
    }
}
